Description: disable download of GSON jar
Author: tony mancill <tmancill@debian.org>
Forwarded: not-needed

--- a/core/build.sh
+++ b/core/build.sh
@@ -8,10 +8,11 @@
 
 MAIN_CLASS=proguard.ProGuard
 
-GSON_VERSION=2.8.5
-GSON_URL=https://jcenter.bintray.com/com/google/code/gson/gson/${GSON_VERSION}/gson-${GSON_VERSION}.jar
-GSON_JAR=$LIB/gson-${GSON_VERSION}.jar
+GSON_JAR=/usr/share/java/gson.jar
+#GSON_VERSION=2.8.5
+#GSON_URL=https://jcenter.bintray.com/com/google/code/gson/gson/${GSON_VERSION}/gson-${GSON_VERSION}.jar
+#GSON_JAR=$LIB/gson-${GSON_VERSION}.jar
 
-download  "$GSON_URL" "$GSON_JAR" && \
+#download  "$GSON_URL" "$GSON_JAR" && \
 compile   $MAIN_CLASS "$GSON_JAR" && \
 createjar "$PROGUARD_JAR" || exit 1
